# **Watch!T - video on demand**

#### **1. Overview**

Fully operational web application for watching movies on demand.

#### **2. Goal**

Main goal for creating this project was to show my abilities to create enterprise software applications in Java technology.<br>
Taking this as a main goal, the focus was put on backend programming.

#### **3. Author**

You can find more information about me on my website (www.grzegorznowosad.pl)

#### **4. Technology stack**
Some of the main technologies that were used:

Backend:
- Java Core
- Java 8
- Java Enterprise Edition (JavaEE, JEE)
- Servlets
- Glassfish 5
- JDBC
- MySQL 5.7

Frontend:
- JSP
- HTML
- Bootstrap 4
- CSS


and additionally some of every-day tools like

- IntelliJ
- maven
- GitLab

#### **5. How to use Watch!T**

- login in, registering

First what you need to do is to sign in to Watch!T. For not logged users only the welcome (main page) is available.
To sign in into the service use Your credentials or register for Your own new account.

There have been already created 2 accounts for Your convenience. You can use them to explore world of movies on Watch!T.


```
user:   john
password: john123
```

or

```
user:   admin
password: admin
```

- movies catalog

After successfully login in You can use website full functionality.<br>
In movies catalog You can browse available movies, sort by title or director and finally rent specific movie.
Rented movie is visible in Your movies section

- profile edit

You are able to edit Your profile

- admin account
	- add / edit / delete movie
	- edit / delete user
	

Being logged as admin gives You unique opportunity to managed movies (add, edit, delete) and users (edit, delete).

Thank You for spending time using Watch!T. I really appreciate feedback so please do not hestitate to share with me Your thoughts and suggestions.
