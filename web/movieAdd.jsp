<%@ page import="java.util.List" %>
<%@ page import="com.watchit.model.Movie" %>
<%@ page import="com.watchit.model.MovieType" %>

<%--
  Created by IntelliJ IDEA.
  User: Grzesiek
  Date: 2018-07-14
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WatchIT - administration - add movie</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/product.css" rel="stylesheet">

</head>
<body>

<%@ include file="layout/header.jsp" %>

<% List<MovieType> listOfMovieTypes = (List<MovieType>) request.getAttribute("listOfMovieTypes");%>

<div class="row mt-4 my-5">
    <div class="col-md-6 col-md-offset-3 text-center p-lg-4 mx-auto">
<br>
        <% if(request.getParameter("error") != null){
        %>
        <%@ include file="layout/errorWrongInput.jsp" %>
        <%
            }%>

        <div class="form-group col-md-4 col-md-offset-2 mx-auto">

            <h2>Add movie</h2><br>

            <form name="groove" method="post" action="manageMovies" onsubmit="return validateForm()">
                <label for="title">Title</label>
                <input class="form-control" name="title" id="title" type="text"><br>

                <label for="director">Director</label>
                <input class="form-control" name="director" id="director" type="text"><br>

                <label for="genre">Genre</label>
                <select class="custom-select mr-sm-2" name="genre" id="genre"><br>
                    <option selected>Choose movie genre</option>
                    <%
                        for(MovieType movietype : listOfMovieTypes){
                    %>
                    <option value="<%=movietype.getType()%>"><%=movietype.getType()%></option>
                    <%
                        }

                    %>
                </select><br><br>
                <label for="description">Description</label>
                <input class="form-control" name="description" id="description" type="text"><br>

                <label for="year">Year</label>
                <input class="form-control" name="year" id="year" type="text"><br>
                <button type="submit" class="btn btn-outline-info mt-3">Add movie</button>


            </form>

        </div>
    </div>
</div>
<%--<%@include file="layout/footer.jsp" %>--%>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
</body>
</html>