<%@ page import="java.util.List" %>
<%@ page import="com.watchit.model.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>WatchIT !</title>

  <!-- Bootstrap core CSS -->
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
  <link rel="stylesheet" href="dist/css/bootstrap.min.css" crossorigin="anonymous">

  <!-- Custom styles for this template -->
  <link href="css/product.css" rel="stylesheet">

    <%--modal ladujacy sie na powitanie--%>

</head>

<body>

<%@ include file="layout/header.jsp" %>

<%
  List<MovieRentedQuantityDTO> listOfMoviesRentedQuantity = (List<MovieRentedQuantityDTO>) request.getAttribute("listOfMoviesRentedQuantity");
  List<MovieGenreDTO> listOfMoviesGenreQuantity = (List<MovieGenreDTO>) request.getAttribute("listOfMoviesGenreQuantity");
  List<DirectorQuantityDTO> listOfDirectorsQuantity = (List<DirectorQuantityDTO>) request.getAttribute("listOfDirectorsQuantity");
  List<UserRentedMoviesQuantityDTO> listOfUsersRentedMoviesQuantity = (List<UserRentedMoviesQuantityDTO>) request.getAttribute("usersRentedMoviesQuantity");
%>

<div class="row mt-4">
  <div class="p-lg-5 mx-auto my-5 col-md-6 col-md-offset-3 text-center">
    <h1 class="display-4 font-weight-normal">Watch!T</h1>
    <p class="lead font-weight-normal">Watch hundred of movies on demand whenever You want</p>
    <br>
    <p class="lead font-weight-normal"></p>

    <div class="container">


    <div class="row">


      <div class="col">
        <p>Most popular movies</p>
        <ul class="list-group">
        <%
          for(MovieRentedQuantityDTO movieRentedQuantityDTO : listOfMoviesRentedQuantity){
          %>
          <li class="list-group-item d-flex justify-content-between align-items-center">
            <%=movieRentedQuantityDTO.getTitle()%>
            <span class="badge badge-primary badge-pill"><%=movieRentedQuantityDTO.getQuantity()%></span>
          </li>
        <%
          }
        %>
        </ul>


      </div>
      <div class="col">
        <p>Most popular genres</p>
        <%
          for(MovieGenreDTO movieGenreDTO : listOfMoviesGenreQuantity){
        %>
        <li class="list-group-item d-flex justify-content-between align-items-center">
          <%=movieGenreDTO.getGenre()%>
          <span class="badge badge-primary badge-success"><%=movieGenreDTO.getQuantity()%></span>
        </li>
        <%
          }
        %>

      </div>


    </div>
        <div class="row">
            <div class="col"><br><br>
                <p>Most popular directors by rent</p>
                <ul class="list-group">
                    <%
                        for(DirectorQuantityDTO directorQuantityDTO : listOfDirectorsQuantity){
                    %>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <%=directorQuantityDTO.getDirectorName()%>
                        <span class="badge badge-primary badge-success"><%=directorQuantityDTO.getQuantity()%></span>
                    </li>
                    <%
                        }
                    %>
                </ul>


            </div>
            <div class="col"><br><br>

                <p>Users with most movies rented</p>
                <ul class="list-group">
                    <%
                        for(UserRentedMoviesQuantityDTO userRentedMoviesQuantityDTO : listOfUsersRentedMoviesQuantity){
                    %>
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <%=userRentedMoviesQuantityDTO.getUserlogin()%>
                        <span class="badge badge-primary badge-pill"><%=userRentedMoviesQuantityDTO.getNumberOfMoviesRented()%></span>
                    </li>
                    <%
                        }
                    %>
                </ul>

            </div>
        </div>

    </div>



      <%
      if(session.getAttribute("login") == null){
          %>
      <!-- As welcome -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLongTitle">Welcome to Watch!T</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                      </button>
                  </div>
                  <div class="modal-body">
                      Fully operational web application for watching movies on demand.<br><br>

                      To use Watch!T You should be successfully logged with credentials. For that You can either:<br><br>
                      a) create Your own account<br>
                      b) use one of already created accounts:<br><br>
                      <table class="table">
                          <thead class="thead-dark">
                          <tr style="text-align: center">
                              <th>user</th>
                              <th>password</th>
                          </tr></thead>
                          <tr style="text-align: center">
                              <td>john</td>
                              <td>john123</td>
                          </tr>
                          <tr style="text-align: center">
                              <td>admin</td>
                              <td>admin</td>
                          </tr>
                      </table>

                      Watch!T is created with:<br>
                      <p style="font-family: Verdana; font-weight: bold; font-size: medium">Java 8, JavaEE, Servlet, JDBC, MySQL 5.7, JSP, <br>HTML, CSS, Bootstrap 4</p><br>

                      More information about this app (technical specification, full source code) can be found on my <a href="https://gitlab.com/groove_grzes/WatchIT">GitLab account</a> . <br>Feel free to contact me.
                  </div>
                  <div class="modal-footer">
                      <button type="button" class="btn btn-success" data-dismiss="modal">Enjoy WatchIT</button>
                  </div>
              </div>
          </div>
      </div>



      <%
      }
      %>

    </div>
  </div>

<%@include file="layout/footer.jsp" %>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>

<script type="text/javascript">
    $(window).on('load',function(){
        $('#myModal').modal('show');
    });
</script>

</body>
</html>
