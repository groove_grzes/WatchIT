<%@ page import="java.util.List" %>
<%@ page import="com.watchit.model.Movie" %>
<%@ page import="com.watchit.model.RentalDTO" %>

<%--
  Created by IntelliJ IDEA.
  User: Grzesiek
  Date: 2018-07-14
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WatchIT - my movies</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/product.css" rel="stylesheet">

</head>
<body>

<%@ include file="layout/header.jsp" %>

<%
    List<RentalDTO> listOfAllMoviesByCustomer = (List<RentalDTO>) request.getAttribute("listOfAllMoviesByCustomer");
%>
<div class="row">
    <div class="p-lg-5 mx-auto my-5 col-md-8 col-md-offset-8 text-center">
        <div class="table-responsive">
            <h3 class="display-5 font-weight-normal">My movies</h3><br>
            <table class="table">
                <tr>
                    <th>title</th>
                    <th>director</th>
                    <th>genre</th>
                    <th>year</th>
                    <th>rental date</th>
                    <th></th>
                    <th></th>

                </tr>

                <%
                    for (RentalDTO rental : listOfAllMoviesByCustomer) {
                %>
                <tr>
                    <td><%=rental.getTitle()%>
                    </td>
                    <td><%=rental.getDirector()%>
                    </td>
                    <td><%=rental.getGenre()%>
                    </td>
                    <td><%=rental.getYear()%>
                    </td>
                    <td><%=rental.getStartDate()%>
                    </td>
                    <%--<td><a class="btn btn-outline-primary" href="#">WATCH</a></td>--%>

                    <!-- Button trigger modal -->
                    <td>
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#id<%=rental.getMovieID()%>">
                            WATCH
                        </button>
                    </td>

                    <!-- Modal -->
                    <div class="modal fade" id="id<%=rental.getMovieID()%>" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel"><%=rental.getTitle()%>
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div style="position:relative;height:0;padding-bottom:56.21%">
                                        <iframe src="https://www.youtube.com/embed/h8CgWxddV6k?ecver=2"
                                                style="position:absolute;width:100%;height:100%;left:0" width="641"
                                                height="360" frameborder="0" allow="autoplay; encrypted-media"
                                                allowfullscreen></iframe>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <a href="myMovies">
                                        <button type="button" class="btn btn-secondary">Close</button>
                                    </a>

                                </div>
                            </div>
                        </div>
                    </div>

                    <td><a class="btn btn-warning" href="movieReturn?id=<%=rental.getMovieID()%>">Return</a></td>

                </tr>
                <%
                    }
                %>
            </table>
        </div>
    </div>
</div>
<%@include file="layout/footer.jsp" %>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
</body>
</html>