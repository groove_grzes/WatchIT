<%@ page import="com.watchit.model.User" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Grzesiek
  Date: 2018-07-14
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WatchIT - login</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/product.css" rel="stylesheet">

</head>
<body>

<%@ include file="layout/header.jsp" %>

<div class="row mt-4 my-5">
    <div class="col-md-6 col-md-offset-3 text-center p-lg-4 mx-auto">

        <br>
        <% if(request.getParameter("error") != null){
        %>
        <%@ include file="layout/errorWrongInput.jsp" %>
        <%
            }%>

        <div class="form-group col-md-4 col-md-offset-2 mx-auto">
            <br>
            <h2>Use Your credentials</h2><br>

            <form method="post" action="login">
                <label for="login">Login</label>
                <input class="form-control" name="login" id="login" type="text"><br>
                <label for="password">Password</label>
                <input class="form-control" name="password" id="password" type="password"><br>

                <button type="submit" class="btn btn-outline-info mt-3">Log in</button>

            </form>
            <a href="register.jsp"><button type="submit" class="btn btn-outline-info mt-3">Register</button></a>

        </div>
    </div>
</div>
<%--<%@include file="layout/footer.jsp" %>--%>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
</body>
</html>