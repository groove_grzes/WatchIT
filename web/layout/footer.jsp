<%--
  Created by IntelliJ IDEA.
  User: Grzesiek
  Date: 2018-07-14
  Time: 09:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>

</head>
<body>
<footer class="container py-5">
    <div class="row">
        <div class="col-4 col-md">
            <%--<h5>Something</h5>--%>
            <%--<ul class="list-unstyled text-small">--%>
                <%--<li><a class="text-muted" href="#">aaaa</a></li>--%>
                <%--<li><a class="text-muted" href="#">bbbb</a></li>--%>
            <%--</ul>--%>

    <small class="d-block mb-3 text-muted">&copy; 2018</small>
    <small class="d-block mb-3 text-muted">Created by</small>
    <small class="d-block mb-3 text-muted">Grzegorz Nowosad</small>
    <small class="d-block mb-3 text-muted"><a href="https://gitlab.com/groove_grzes"><img src="gfx/gitlab-stacked_wm_no_bg-300x300.png" width="50px" height="50px"></a> </small>


    </div>
        <div class="col-4 col-md">
            <h5>Resources</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="movies">Movies catalog</a></li>
            </ul>
        </div>
        <div class="col-4 col-md">
            <h5>Other projects</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="#">WatchIT</a></li>
                <li><a class="text-muted" href="#">DiscussIT</a></li>

            </ul>
        </div>
        <div class="col-6 col-md">
            <h5>About</h5>
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="http://www.grzegorznowosad.pl">Contact me</a></li>

            </ul>
        </div>
    </div>
</footer>
</body>
</html>
