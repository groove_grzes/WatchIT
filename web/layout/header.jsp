    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
        <html>
        <head>
        <title>header</title>
        </head>
        <body>

            <%
            String userLogin = (String) session.getAttribute("login");
        %>
        <nav class="site-header sticky-top py-1">
        <div class="container d-flex flex-column flex-md-row justify-content-between">
        <a class="py-2 d-none d-md-inline-block" href="index.jsp">Watch!T</a>
        <a class="py-2 d-none d-md-inline-block" href="movies">Movies Catalog</a>
        <a class="py-2 d-none d-md-inline-block" href="myMovies">My movies</a>

            <%if("admin".equals(userLogin)){
        %>

        <a class="py-2 d-none d-md-inline-block" href="manageMovies">Manage Movies</a>
        <a class="py-2 d-none d-md-inline-block" href="users">Manage Users</a>
            <%
    }%>

            <%if(userLogin != null){
        %>
            <%-- user jest zalogowany--%>
        <div class="dropdown">
        <button class="btn btn-outline-danger dropdown-toggle" style="color: white" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <%=userLogin%>
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
        <a class="dropdown-item" href="myProfile">My Profile</a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="logout">Logout</a>
        </div>
        </div>

            <%
    }else{
      %>
        <%-- user nie jest zalogowany--%>
        <div class="dropdown">
            <button class="btn btn-outline-danger dropdown-toggle" style="color: white"  type="button" id="dropdownMenuButton" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
                Your Account
            </button>
            <div class="dropdown-menu ">
                <form class="px-4 py-3 " method="post" action="login">
                <div class="form-group">
                    <label for="login">Login</label>
                    <input type="text" class="form-control" name="login" id="login" placeholder="Your user login">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-primary">Sign in</button>
                </form>
                <div class="dropdown-divider"></div>
            <a class="dropdown-item menu-dropdown-link" href="register.jsp">New to WatchIT? Sign up</a>
            </div>
        </div>
            <%
    }
    %>

        </div>
        </nav>


        <script src="dist/js/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

        </body>
        </html>
