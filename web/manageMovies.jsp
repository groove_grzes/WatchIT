<%@ page import="java.util.List" %>
<%@ page import="com.watchit.model.Movie" %>

<%--
  Created by IntelliJ IDEA.
  User: Grzesiek
  Date: 2018-07-14
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WatchIT - administration - managing movies</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/product.css" rel="stylesheet">

</head>
<body>

<%@ include file="layout/header.jsp" %>

<%
    List<Movie> listOfAllMovies = (List<Movie>) request.getAttribute("listOfAllMovies");
%>
<div class="row">
    <div class="p-lg-5 mx-auto my-5 col-md-8 col-md-offset-8 text-center">
        <div class="table-responsive">
            <h3 class="display-5 font-weight-normal">Manage movies</h3><br>
            <table class="table">
    <tr>
        <th>title</th>
        <th>director</th>
        <th>genre</th>
        <th>year</th>
        <th>description</th>
        <th></th>
        <th></th>
    </tr>

<%
    for(Movie movie : listOfAllMovies){
%>
    <tr>
        <td><%=movie.getTitle()%></td>
        <td><%=movie.getDirector()%></td>
        <td><%=movie.getGenre()%></td>
        <td><%=movie.getYear()%></td>
        <td><%=movie.getDescription()%></td>
        <td><a class="btn btn-warning" href="moviesEdit?id=<%=movie.getId()%>">Edit</a></td>
        <td><a class="btn btn-danger" href="moviesDelete?id=<%=movie.getId()%>">Delete</a></td>
    </tr>
<%
    }
%>
            </table>
        </div>
        <div class="table-responsive">
            <%--<h3 class="display-5 font-weight-normal">Add New Movie--%>
            <a class="btn btn-primary btn-lg btn-block" href="movieAdd">add new movie</a>
            </h3>

        </div>
        </div>
    </div>
</div>
<%@include file="layout/footer.jsp" %>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
</body>
</html>