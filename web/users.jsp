<%@ page import="com.watchit.model.User" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WatchIT - administration - manage users</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/product.css" rel="stylesheet">

</head>
<body>

<%@ include file="layout/header.jsp" %>

<%
    List<User> listOfAllUsers = (List<User>) request.getAttribute("listOfAllUsers");
%>
<div class="row">
    <div class="p-lg-5 mx-auto my-5 col-md-6 col-md-offset-3 text-center">
        <div class="table-responsive">
            <h3 class="display-5 font-weight-normal">Registered users</h3><br>
            <table class="table">
    <tr>
        <th>login</th>
        <th>first name</th>
        <th>last name</th>
        <th>birth Year</th>
        <th></th>
        <th></th>
    </tr>

<%
    for(User user : listOfAllUsers){
%>
    <tr>
        <td><%=user.getLogin()%></td>
        <td><%=user.getFirstName()%></td>
        <td><%=user.getLastName()%></td>
        <td><%=user.getBirthYear()%></td>
        <td><a class="btn btn-warning" href="profileEdit?login=<%=user.getLogin()%>">Edit</a></td>
        <td><a class="btn btn-danger" href="userDelete?login=<%=user.getLogin()%>">Delete</a></td>
    </tr>
<%
    }
%>
            </table>
        </div>
    </div>
</div>

<%@include file="layout/footer.jsp" %>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
</body>
</html>