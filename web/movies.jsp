<%@ page import="java.util.List" %>
<%@ page import="com.watchit.model.Movie" %>
<%@ page import="com.watchit.model.MovieRentedBy" %>

<%--
  Created by IntelliJ IDEA.
  User: Grzesiek
  Date: 2018-07-14
  Time: 10:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>WatchIT - available movies</title>

    <!-- Bootstrap core CSS -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/bootstrap.min.css" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="css/product.css" rel="stylesheet">

</head>
<body>

<%@ include file="layout/header.jsp" %>
<%
    List<MovieRentedBy> listOfMovies = (List<MovieRentedBy>)request.getAttribute("listOfMovies");

    String byTitle = "TitleAZ";
    String byDirector = "DirectorAZ";
    String iconTitle = "fa fa-sort-alpha-asc";
    String iconDirector = "fa fa-sort-alpha-asc";

    if(request.getParameter("sortBy") == null){
        byTitle = "TitleAZ";
        byDirector = "DirectorAZ";
        iconTitle = "fa fa-sort-alpha-asc";
        iconDirector = "fa fa-sort-alpha-asc";
    }else if(request.getParameter("sortBy").equals("TitleAZ")){
        byTitle = "TitleZA";
        iconTitle = "fa fa-sort-alpha-desc";
    }else if(request.getParameter("sortBy").equals("TitleZA")){
        byTitle = "TitleAZ";
        iconTitle = "fa fa-sort-alpha-asc";
    }else if(request.getParameter("sortBy").equals("DirectorAZ")){
        byDirector = "DirectorZA";
        iconDirector = "fa fa-sort-alpha-desc";
    }else if(request.getParameter("sortBy").equals("DirectorZA")){
        byDirector = "DirectorAZ";
        iconDirector = "fa fa-sort-alpha-asc";
    }

%>

<div class="row">
    <div class="p-lg-5 mx-auto my-5 col-md-6 col-md-offset-3 text-center">
        <div class="table-responsive">
            <h3 class="display-5 font-weight-normal">Available movies</h3><br>
            <table class="table">
                <thead>
                <tr>
                    <th>Title
                        <a href="movies?sortBy=<%=byTitle%>">
                            <%--<i class="fa fa-sort-alpha-asc"></i>--%>
                            <i class="<%=iconTitle%>"></i>
                        </a>
                    </th>
                    <th>Director
                        <a href="movies?sortBy=<%=byDirector%>">
                            <%--<i class="fa fa-sort-alpha-asc"></i>--%>
                                <i class="<%=iconDirector%>"></i>
                        </a>

                    </th>
                    <th>Genre</th>
                    <th>Year</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>

                <% for(MovieRentedBy movie : listOfMovies){ %>



                <tr>
                    <td scope="row"><%=movie.getTitle()%></td>
                    <td><%=movie.getDirector()%></td>
                    <td><span class="badge badge-info"><%=movie.getGenre()%></span></td>
                    <td><%=movie.getYear()%></td>
                    <td>

                        <%
                            if(movie.getUser() == null){
                        %>
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#id<%=movie.getMovieID()%>">
                            Rent
                        </button>
                        <div class="modal fade" id="id<%=movie.getMovieID()%>" tabindex="-1" role="dialog" aria-labelledby="<%=movie.getMovieID()%>ModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="<%=movie.getMovieID()%>ModalLabel"><%=movie.getTitle()%></h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <%=movie.getDescription()%>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <%--<button type="button" class="btn btn-primary" href="rent">Rent movie</button>--%>
                                        <a class="btn btn-primary" href="rent?id=<%=movie.getMovieID()%>" role="button">Rent</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%
                        }else{
                        %>

                        <%--<span class="badge badge-success">Already rented!</span>--%>

                        <button type="button" class="btn btn-success">
                            Rented
                        </button>

                        <%
                            }

                        %>
                    </td>

                    <!-- Modal -->


                    <%
                        }
                    %>

                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>


<%@include file="layout/footer.jsp" %>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
</body>
</html>