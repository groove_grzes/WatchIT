package com.watchit.DAO;

import com.watchit.model.MovieRentedBy;
import com.watchit.model.Rental;
import com.watchit.model.RentalDTO;

import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class RentalsDAO {

    static{
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<Rental> getAllRentals(){

        List<Rental> listOfAllRentals = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select * from rentals");
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                Integer id = rs.getInt("id");
                Integer movieID = rs.getInt("movieID");
                String userLogin = rs.getString("userLogin");
                Date startDate = rs.getDate("startDate");
                Date endDate = rs.getDate("endDate");

                listOfAllRentals.add(new Rental(id, movieID, userLogin, startDate, endDate));
            }

        }catch (SQLException exgetAllRentals){
            System.out.println(exgetAllRentals);
        }

        return listOfAllRentals;
    }

    public void create(String userLogin, Integer movieID){

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO rentals SET movieId = ?, userLogin = ?");
            preparedStatement.setInt(1, movieID);
            preparedStatement.setString(2, userLogin);
            preparedStatement.executeUpdate();


        }catch (SQLException excreateCRental){
            System.out.println(excreateCRental);
        }

    }

    public List<RentalDTO> getMoviesByUser(String userLogin){
        List<RentalDTO> listOfMoviesByUser = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select movieId,userLogin, title, director, genre, year, startDate from rentals r left join movies m on r.movieId = m.id where r.userLogin = ?;");
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                int movieID = rs.getInt("movieID");
                 String title = rs.getString("title");
                 String director = rs.getString("director");
                 String genre = rs.getString("genre");
                 int year = rs.getInt("year");
                 Date startDate = rs.getDate("startDate");

                 listOfMoviesByUser.add(new RentalDTO(movieID,title, director, genre, year, startDate));

            }
        }catch (SQLException exgetMoviesByUser){
            System.out.println(exgetMoviesByUser);
        }

        return listOfMoviesByUser;
    }

    public void deleteRental(int movieID){
        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("delete from rentals where movieId = ?");
            preparedStatement.setInt(1, movieID);
            preparedStatement.executeUpdate();


        }catch (SQLException exdeleteRental){
            System.out.println(exdeleteRental);
        }
    }


    // trzeba dodac cialo tej metody
    public void deleteAllRentalsByUser(String userLogin){

    }

    public List<MovieRentedBy> getMovies(String userLogin){
        List<MovieRentedBy> listOfMovies = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select id, title, director, m.description ,genre,  year, userLogin from movies m left join (select movieId, userLogin from rentals where userLogin = ?) as r on m.id = r.movieId;");
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                Integer movieID = rs.getInt("id");
                String title = rs.getString("title");
                String director = rs.getString("director");
                String genre = rs.getString("genre");
                int year = rs.getInt("year");
                String user = (String) rs.getObject("userLogin");
                String description = rs.getString("m.description");

                listOfMovies.add(new MovieRentedBy(movieID, title, director, genre, description, year, user));
            }

        }catch (SQLException exgetMovies){
            System.out.println(exgetMovies);
        }

        return listOfMovies;
    }

    public List<MovieRentedBy> getMoviesByTitleAZ(String userLogin){
        List<MovieRentedBy> listOfMovies = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select id, title, director, m.description ,genre,  year, userLogin from movies m left join (select movieId, userLogin from rentals where userLogin = ?) as r on m.id = r.movieId order by title asc;");
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                Integer movieID = rs.getInt("id");
                String title = rs.getString("title");
                String director = rs.getString("director");
                String genre = rs.getString("genre");
                int year = rs.getInt("year");
                String user = (String) rs.getObject("userLogin");
                String description = rs.getString("m.description");

                listOfMovies.add(new MovieRentedBy(movieID, title, director, genre, description, year, user));
            }

        }catch (SQLException exgetMoviesByGenre){
            System.out.println(exgetMoviesByGenre);
        }

        return listOfMovies;
    }

    public List<MovieRentedBy> getMoviesByDirectorAZ(String userLogin){
        List<MovieRentedBy> listOfMovies = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select id, title, director, m.description ,genre,  year, userLogin from movies m left join (select movieId, userLogin from rentals where userLogin = ?) as r on m.id = r.movieId order by director asc;");
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                Integer movieID = rs.getInt("id");
                String title = rs.getString("title");
                String director = rs.getString("director");
                String genre = rs.getString("genre");
                int year = rs.getInt("year");
                String user = (String) rs.getObject("userLogin");
                String description = rs.getString("m.description");

                listOfMovies.add(new MovieRentedBy(movieID, title, director, genre, description, year, user));
            }

        }catch (SQLException exgetMoviesByDirector){
            System.out.println(exgetMoviesByDirector);
        }

        return listOfMovies;
    }

    public List<MovieRentedBy> getMoviesByTitleZA(String userLogin){
        List<MovieRentedBy> listOfMovies = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select id, title, director, m.description ,genre,  year, userLogin from movies m left join (select movieId, userLogin from rentals where userLogin = ?) as r on m.id = r.movieId order by title desc ;");
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                Integer movieID = rs.getInt("id");
                String title = rs.getString("title");
                String director = rs.getString("director");
                String genre = rs.getString("genre");
                int year = rs.getInt("year");
                String user = (String) rs.getObject("userLogin");
                String description = rs.getString("m.description");

                listOfMovies.add(new MovieRentedBy(movieID, title, director, genre, description, year, user));
            }

        }catch (SQLException exgetMoviesByGenre){
            System.out.println(exgetMoviesByGenre);
        }

        return listOfMovies;
    }

    public List<MovieRentedBy> getMoviesByDirectorZA(String userLogin){
        List<MovieRentedBy> listOfMovies = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select id, title, director, m.description ,genre,  year, userLogin from movies m left join (select movieId, userLogin from rentals where userLogin = ?) as r on m.id = r.movieId order by director desc ;");
            preparedStatement.setString(1, userLogin);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                Integer movieID = rs.getInt("id");
                String title = rs.getString("title");
                String director = rs.getString("director");
                String genre = rs.getString("genre");
                int year = rs.getInt("year");
                String user = (String) rs.getObject("userLogin");
                String description = rs.getString("m.description");

                listOfMovies.add(new MovieRentedBy(movieID, title, director, genre, description, year, user));
            }

        }catch (SQLException exgetMoviesByDirector){
            System.out.println(exgetMoviesByDirector);
        }

        return listOfMovies;
    }
}
