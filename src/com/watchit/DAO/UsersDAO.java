package com.watchit.DAO;

import com.watchit.model.User;

import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class UsersDAO {

    static{
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<User> getAllUsers(){

        List<User> listOfAllUsers = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            Statement st = conn.createStatement();

            ResultSet rs = st.executeQuery("select * from users");

            while(rs.next()){
                 String login = rs.getString("login");
                 String passwordr = rs.getString("password");
                 String firstName = rs.getString("firstName");
                 String lastName = rs.getString("lastName");
                 int birthYear = Integer.parseInt(rs.getString("birthYear"));

                 listOfAllUsers.add(new User(login, passwordr, firstName,lastName, birthYear));
            }

        }catch(SQLException exgetAllUsers){
            System.out.println(exgetAllUsers);
        }

        return listOfAllUsers;
    }

    public User getUserByLogin(String loginToFind){

        User userToReturn = null;

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select * from users where login = ?");
            preparedStatement.setString(1, loginToFind);
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                String login = rs.getString("login");
                String passwordr = rs.getString("password");
                String firstName = rs.getString("firstName");
                String lastName = rs.getString("lastName");
                int birthYear = Integer.parseInt(rs.getString("birthYear"));

                userToReturn = new User(login, passwordr, firstName, lastName, birthYear);
            }

        }catch(SQLException exgetUserByLogin){
            System.out.println(exgetUserByLogin);
        }

        return userToReturn;
    }

    public void createUser(User user){
        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("insert into users values (?,?,?,?,?)");
            preparedStatement.setString(1, user.getLogin());
            preparedStatement.setString(2, user.getPassword());
            preparedStatement.setString(3, user.getFirstName());
            preparedStatement.setString(4, user.getLastName());
            preparedStatement.setInt(5, user.getBirthYear());
            preparedStatement.executeUpdate();


        }catch(SQLException excreateUser){
            System.out.println(excreateUser);
        }
    }

    public void updateUser(User user){
        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("update users set firstName = ?, lastName = ?, password = ?, birthYear = ? where login = ?;");

            preparedStatement.setString(1, user.getFirstName());
            preparedStatement.setString(2, user.getLastName());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setInt(4, user.getBirthYear());
            preparedStatement.setString(5, user.getLogin());
            preparedStatement.executeUpdate();

        }catch(SQLException exupdateUser){
            System.out.println(exupdateUser);
        }
    }

    public void deleteUser(String loginToDelete){

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("delete from users where login = ?");
            preparedStatement.setString(1, loginToDelete);
            preparedStatement.executeUpdate();


        }catch(SQLException exdeleteUser){
            System.out.println(exdeleteUser);
        }
    }
}
