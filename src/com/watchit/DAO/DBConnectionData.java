package com.watchit.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnectionData {

    static final String BUG = "?autoReconnect=true&useSSL=false&serverTimezone=UTC";
    static final String DBUSER = "root";
    static final String DBPASSWORD = "";
    static final String SERVER = "jdbc:mysql://127.0.0.1:3306/watchit";

    static public Connection initiateConnection() throws SQLException {

        return DriverManager.getConnection(SERVER + BUG, DBUSER, DBPASSWORD);
    }
}
