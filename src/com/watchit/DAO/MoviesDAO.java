package com.watchit.DAO;

import com.watchit.model.*;

import javax.ejb.Stateless;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Stateless
public class MoviesDAO {

    DBConnectionData dbConnectionData = new DBConnectionData();

    static{
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<Movie> getAllMovies(){
        List<Movie> listOfAllMovies = new ArrayList<>();

//        try(Connection conn = DriverManager.getConnection(DBConnectionData.SERVER + DBConnectionData.BUG, DBConnectionData.DBUSER, DBConnectionData.DBPASSWORD)){
        try(Connection conn = DBConnectionData.initiateConnection()){

           PreparedStatement preparedStatement = conn.prepareStatement("select * from movies");
           ResultSet rs = preparedStatement.executeQuery();

           while(rs.next()){
               int id = Integer.parseInt(rs.getString("id"));
               String title = rs.getString("title");
               String director = rs.getString("director");
               String genre = rs.getString("genre");
               String description = rs.getString("description");
               int year = Integer.parseInt(rs.getString("year"));

               listOfAllMovies.add(new Movie(id, title, genre, description, year, director));
           }
        }catch (SQLException exGetAllMovies){
            System.out.println(exGetAllMovies);
        }

        return listOfAllMovies;
    }

    public Movie getMovieById(int id){
        Movie movieTOReturn = null;

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select * from movies where id = ?");
            preparedStatement.setString(1, String.valueOf(id));
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                int idToReturn = Integer.parseInt(rs.getString("id"));
                String title = rs.getString("title");
                String director = rs.getString("director");
                String genre = rs.getString("genre");
                String description = rs.getString("description");
                int year = Integer.parseInt(rs.getString("year"));

                movieTOReturn = new Movie(idToReturn, title, genre, description, year, director);
            }
        }catch (SQLException exGetMovieById){
            System.out.println(exGetMovieById);
        }

        return movieTOReturn;
    }

    public void createMovie(Movie movieToAdd){
        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("insert into movies values (?, ?, ?, ?, ?, ?)");

            preparedStatement.setObject(1, movieToAdd.getId());
            preparedStatement.setString(2, movieToAdd.getTitle());
            preparedStatement.setString(3, movieToAdd.getDirector());
            preparedStatement.setString(4, movieToAdd.getGenre());
            preparedStatement.setString(5, movieToAdd.getDescription());
            preparedStatement.setInt(6, movieToAdd.getYear());
            preparedStatement.executeUpdate();

        }catch (SQLException exCreateMovie){
            System.out.println(exCreateMovie);
        }
    }

    public void updateMovie(Movie movieToUpdate){
        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("update movies set title = ?, director = ?, genre = ?, description = ?, year = ? where id = ?;");
            preparedStatement.setString(1, movieToUpdate.getTitle());
            preparedStatement.setString(2, movieToUpdate.getDirector());
            preparedStatement.setString(3, movieToUpdate.getGenre());
            preparedStatement.setString(4, movieToUpdate.getDescription());
            preparedStatement.setString(5, String.valueOf(movieToUpdate.getYear()));
            preparedStatement.setString(6, String.valueOf(movieToUpdate.getId()));

            preparedStatement.executeUpdate();

        }catch (SQLException exUpdateMovie){
            System.out.println(exUpdateMovie);
        }
    }

    public void deleteMovie(int idToDelete){
        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("delete from movies where id = ?");
            preparedStatement.setString(1, String.valueOf(idToDelete));

            preparedStatement.executeUpdate();

        }catch (SQLException exDeleteMovie){
            System.out.println(exDeleteMovie);
        }
    }

    public List<MovieType> movieTypes(){
        List<MovieType> listOfMovieTypes = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select * from movietypes");
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                int id = Integer.parseInt(rs.getString("typeID"));
                String type = rs.getString("Type");

                listOfMovieTypes.add(new MovieType(id, type));
            }
        }catch (SQLException exMovieTypes){
            System.out.println(exMovieTypes);
        }

        return listOfMovieTypes;
    }

    public List<MovieRentedQuantityDTO> moviesRentedQuantity(){
        List<MovieRentedQuantityDTO> listOfMoviesRentedQuantity = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select m.title, count(r.userLogin) as quantity from rentals r left join movies m on r.movieId = m.id group by m.title order by quantity desc limit 5");
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                String title = rs.getString("title");
                int quantity = Integer.parseInt(rs.getString("quantity"));


                listOfMoviesRentedQuantity.add(new MovieRentedQuantityDTO(title, quantity));
            }
        }catch (SQLException exmoviesRentedQuantity){
            System.out.println(exmoviesRentedQuantity);
        }

        return listOfMoviesRentedQuantity;
    }

    public List<MovieGenreDTO> moviesGenreQuantity(){
        List<MovieGenreDTO> listOfMoviesGenreQuantity = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select m.genre, count(r.userLogin) as quantity from rentals r left join movies m on r.movieId = m.id group by m.genre limit 5;");
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                String genre = rs.getString("genre");
                int quantity = Integer.parseInt(rs.getString("quantity"));

                listOfMoviesGenreQuantity.add(new MovieGenreDTO(genre, quantity));
            }
        }catch (SQLException exmoviesGenreQuantity){
            System.out.println(exmoviesGenreQuantity);
        }

        return listOfMoviesGenreQuantity;
    }

    public List<DirectorQuantityDTO> directorsQuantity(){

        List<DirectorQuantityDTO> listOfDirectorsQuantity = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select m.director, count(r.userLogin) as quantity from rentals r left join movies m on r.movieId = m.id group by m.director order by quantity desc limit 5;");
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                String directorName = rs.getString("director");
                int quantity = Integer.parseInt(rs.getString("quantity"));

                listOfDirectorsQuantity.add(new DirectorQuantityDTO(directorName, quantity));
            }
        }catch (SQLException exdirectorsQuantity){
            System.out.println(exdirectorsQuantity);
        }

        return listOfDirectorsQuantity;
    }

    public List<UserRentedMoviesQuantityDTO> usersRentedMoviesQuantity(){

        List<UserRentedMoviesQuantityDTO> listsOfUsersRentedMovies = new ArrayList<>();

        try(Connection conn = DBConnectionData.initiateConnection()){

            PreparedStatement preparedStatement = conn.prepareStatement("select userLogin, count(movieId) as quantity from rentals group by userLogin order by quantity desc limit 5;");
            ResultSet rs = preparedStatement.executeQuery();

            while(rs.next()){
                String userlogin = rs.getString("userLogin");
                int quantity = Integer.parseInt(rs.getString("quantity"));

                listsOfUsersRentedMovies.add(new UserRentedMoviesQuantityDTO(userlogin, quantity));
            }
        }catch (SQLException exusersRentedMoviesQuantity){
            System.out.println(exusersRentedMoviesQuantity);
        }

        for(UserRentedMoviesQuantityDTO user : listsOfUsersRentedMovies){
            StringBuilder newUserLogin = new StringBuilder();
            newUserLogin.append(user.getUserlogin().charAt(0));

            for (int i = 1; i < user.getUserlogin().length(); i++) {
             newUserLogin.append("*");
            }

            user.setUserlogin(newUserLogin.toString());

        }

        return listsOfUsersRentedMovies;
    }
}
