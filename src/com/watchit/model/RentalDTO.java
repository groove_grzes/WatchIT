package com.watchit.model;

import java.sql.Date;

public class RentalDTO {
    private String title;
    private String director;
    private String genre;
    private int year;
    private int movieID;
    private Date startDate;

    public RentalDTO(int movieID,String title, String director, String genre, int year, Date startDate) {
        this.title = title;
        this.director = director;
        this.genre = genre;
        this.year = year;
        this.startDate = startDate;
        this.movieID = movieID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }
}
