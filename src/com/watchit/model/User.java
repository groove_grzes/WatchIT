package com.watchit.model;

public class User {
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private int birthYear;

    public User(String login, String password, String firstName, String lastName, int birthYear) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthYear = birthYear;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getBirthYear() {
        return birthYear;
    }

    @Override
    public String toString(){
        return this.login + ", haslo: " + this.password + ", " + this.firstName + " " + this.lastName + ", rok urodzenia: " + this.birthYear;
    }
}
