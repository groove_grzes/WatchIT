package com.watchit.model;

public class MovieGenreDTO {
    private String genre;
    private int quantity;

    public MovieGenreDTO(String genre, int quantity) {
        this.genre = genre;
        this.quantity = quantity;
    }

    public String getGenre() {
        return genre;
    }

    public int getQuantity() {
        return quantity;
    }
}
