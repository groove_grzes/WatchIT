package com.watchit.model;

public class DirectorQuantityDTO {
    private String directorName;
    private int quantity;

    public DirectorQuantityDTO(String directorName, int quantity) {
        this.directorName = directorName;
        this.quantity = quantity;
    }

    public String getDirectorName() {
        return directorName;
    }

    public void setDirectorName(String directorName) {
        this.directorName = directorName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
