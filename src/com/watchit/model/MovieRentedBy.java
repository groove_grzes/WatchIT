package com.watchit.model;

public class MovieRentedBy {
    private int movieID;
    private String title;
    private String genre;
    private String director;
    private int year;
    private String user;
    private String description;

    public MovieRentedBy(int movieID, String title, String director, String genre, String description,int year, String user) {
        this.movieID = movieID;
        this.title = title;
        this.genre = genre;
        this.director = director;
        this.description = description;
        this.year = year;
        this.user = user;
    }

    public int getMovieID() {
        return movieID;
    }

    public void setMovieID(int movieID) {
        this.movieID = movieID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getDescription(){
        return this.description;
    }

}
