package com.watchit.model;

public class UserRentedMoviesQuantityDTO {
    private String userlogin;
    private int numberOfMoviesRented;

    public UserRentedMoviesQuantityDTO(String userlogin, int numberOfMoviesRented) {
        this.userlogin = userlogin;
        this.numberOfMoviesRented = numberOfMoviesRented;
    }

    public String getUserlogin() {
        return userlogin;
    }

    public void setUserlogin(String userlogin) {
        this.userlogin = userlogin;
    }

    public int getNumberOfMoviesRented() {
        return numberOfMoviesRented;
    }

    public void setNumberOfMoviesRented(int numberOfMoviesRented) {
        this.numberOfMoviesRented = numberOfMoviesRented;
    }
}
