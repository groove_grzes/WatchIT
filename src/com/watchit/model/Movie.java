package com.watchit.model;

public class Movie {
    private Integer id;
    private String title;
    private String genre;
    private String description;
    private int year;
    private String director;

    public Movie(Integer id, String title, String genre, String description, int year, String director) {
        this.id = id;
        this.title = title;
        this.genre = genre;
        this.description = description;
        this.year = year;
        this.director = director;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    @Override
    public String toString(){
        return id + ", " + title + ", rezyser: " + director + ", opis: " + description + ", rodzaj: " + genre + ", rok: " + year;
    }
}
