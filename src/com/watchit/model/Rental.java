package com.watchit.model;

import java.sql.Date;

public class Rental {
    private Integer id;
    private Integer movieID;
    private String userLogin;
    private Date startDate;
    private Date endDate;

    public Rental(Integer id, Integer movieID, String userLogin, Date startDate, Date endDate) {
        this.id = id;
        this.movieID = movieID;
        this.userLogin = userLogin;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMovieID() {
        return movieID;
    }

    public void setMovieID(Integer movieID) {
        this.movieID = movieID;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
