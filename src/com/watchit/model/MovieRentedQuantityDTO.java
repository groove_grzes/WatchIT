package com.watchit.model;

// this is an DTO class
// this DTO object will represent title of movie with quantity of rented

public class MovieRentedQuantityDTO {
    private String title;
    private int quantity;

    public MovieRentedQuantityDTO(String title, int quantity){
        this.title = title;
        this.quantity = quantity;
    }

    public String getTitle(){
        return this.title;
    }

    public int getQuantity(){
        return this.quantity;
    }
}
