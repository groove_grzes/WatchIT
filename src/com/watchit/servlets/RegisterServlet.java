package com.watchit.servlets;

import com.watchit.DAO.UsersDAO;
import com.watchit.model.User;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

    @EJB
    UsersDAO usersDAO;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String birthYear = req.getParameter("birthYear");

        if(login.isEmpty() || password.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || birthYear.isEmpty()){

            resp.sendRedirect(resp.encodeRedirectURL("register.jsp?error=dataError"));

        }else{

            int birthYearInt = Integer.parseInt(birthYear);
            usersDAO.createUser(new User(login, password, firstName, lastName, birthYearInt));

            resp.sendRedirect(resp.encodeRedirectURL("users"));
        }
    }
}
