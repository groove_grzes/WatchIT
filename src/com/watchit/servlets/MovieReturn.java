package com.watchit.servlets;

import com.watchit.DAO.RentalsDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/movieReturn")
public class MovieReturn extends HttpServlet {

    @EJB
    private RentalsDAO rentalsDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int movieIDtoReturn = Integer.parseInt(req.getParameter("id"));

        rentalsDAO.deleteRental(movieIDtoReturn);

        resp.sendRedirect(resp.encodeRedirectURL("myMovies"));

    }
}
