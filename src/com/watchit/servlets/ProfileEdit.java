package com.watchit.servlets;

import com.watchit.DAO.UsersDAO;
import com.watchit.model.User;
import com.watchit.servlets.other.YearValidation;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/profileEdit")
public class ProfileEdit extends HttpServlet {

    @EJB
    UsersDAO usersDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");

        User userToEdit = usersDAO.getUserByLogin(login);

        req.setAttribute("userToEdit", userToEdit);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("profileEdit.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String year = req.getParameter("birthYear");

        if(isUserDataValid(login, password, firstName, lastName, year)){

            resp.sendRedirect(resp.encodeRedirectURL("profileEdit?error=dataError&login=" + login));

        }else{
            int birthYear = Integer.parseInt(year);
            usersDAO.updateUser(new User(login, password, firstName, lastName, birthYear));

            resp.sendRedirect(resp.encodeRedirectURL("index.jsp"));
        }
    }

    private boolean isUserDataValid(String login, String password, String firstName, String lastName, String year){
        return (login.isEmpty() || password.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || year.isEmpty() || !YearValidation.isYearProper(year));
    }
}
