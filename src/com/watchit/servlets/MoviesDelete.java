package com.watchit.servlets;

import com.watchit.DAO.MoviesDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/moviesDelete")
public class MoviesDelete extends HttpServlet {

    @EJB
    private MoviesDAO moviesDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int idToDelete = Integer.parseInt(req.getParameter("id"));

        moviesDAO.deleteMovie(idToDelete);

        resp.sendRedirect(resp.encodeRedirectURL("manageMovies"));

    }
}
