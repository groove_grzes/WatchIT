package com.watchit.servlets;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter("/*")
public class MyFilter implements Filter {

    final String ALLOWED_SUFFIXES[] = {"login.jsp", "login", ".css", ".js", "register", "register.jsp", ".png", "index.jsp"};

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;

        String requestURI = req.getRequestURI();

        HttpSession session = req.getSession();

        String login = (String) session.getAttribute("login");

        if(login != null || isSuffixValid(requestURI)

                ){
            filterChain.doFilter(req,resp);
        }else{

            resp.sendRedirect(resp.encodeRedirectURL("index.jsp"));
        }
    }

    private boolean isSuffixValid(String requestURI) {

        for (String suffix : ALLOWED_SUFFIXES) {
            if (requestURI.endsWith(suffix)) {
                 return true;
            }
        }
        return false;
    }
}
