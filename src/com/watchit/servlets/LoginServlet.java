package com.watchit.servlets;

import com.watchit.DAO.UsersDAO;
import com.watchit.model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("login");
        String password = req.getParameter("password");

        UsersDAO usersDAO = new UsersDAO();
        User userToCheck = usersDAO.getUserByLogin(username);

        if(userToCheck != null && password.equals(userToCheck.getPassword())){

            HttpSession session = req.getSession();
            session.setAttribute("login", userToCheck.getLogin());
            resp.sendRedirect(resp.encodeRedirectURL("index.jsp"));

        }else{
            resp.sendRedirect(resp.encodeRedirectURL("login.jsp?error=badCredentials"));
        }

    }
}
