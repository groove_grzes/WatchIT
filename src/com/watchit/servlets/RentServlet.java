package com.watchit.servlets;

import com.watchit.DAO.RentalsDAO;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/rent")
public class RentServlet extends HttpServlet {

    @EJB
    RentalsDAO rentalsDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idToRent = req.getParameter("id");
        String userLogin = (String) req.getSession().getAttribute("login");

        rentalsDAO.create(userLogin, Integer.parseInt(idToRent));

        resp.sendRedirect(resp.encodeRedirectURL("movies"));

    }
}
