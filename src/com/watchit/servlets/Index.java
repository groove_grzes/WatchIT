package com.watchit.servlets;

import com.watchit.DAO.MoviesDAO;
import com.watchit.model.*;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/index.jsp")
public class Index extends HttpServlet {

    @EJB
    MoviesDAO moviesDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<MovieRentedQuantityDTO> listOfMoviesRentedQuantity = moviesDAO.moviesRentedQuantity();

        req.setAttribute("listOfMoviesRentedQuantity", listOfMoviesRentedQuantity);

        List<MovieGenreDTO> listOfMoviesGenreQuantity = moviesDAO.moviesGenreQuantity();

        req.setAttribute("listOfMoviesGenreQuantity", listOfMoviesGenreQuantity);

        List<DirectorQuantityDTO> listOfDirectorsQuantity = moviesDAO.directorsQuantity();

        req.setAttribute("listOfDirectorsQuantity", listOfDirectorsQuantity);

        List<UserRentedMoviesQuantityDTO> usersRentedMoviesQuantity = moviesDAO.usersRentedMoviesQuantity();

        req.setAttribute("usersRentedMoviesQuantity", usersRentedMoviesQuantity);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("main.jsp");
        requestDispatcher.forward(req, resp);

    }
}
