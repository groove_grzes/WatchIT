package com.watchit.servlets;

import com.watchit.DAO.UsersDAO;
import com.watchit.model.User;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/myProfile")
public class MyProfile extends HttpServlet {

    @EJB
    UsersDAO usersDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = (String) req.getSession().getAttribute("login");

        User userToPrint = usersDAO.getUserByLogin(username);

        req.setAttribute("userToPrint", userToPrint);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("myProfile.jsp");
        requestDispatcher.forward(req, resp);
    }
}
