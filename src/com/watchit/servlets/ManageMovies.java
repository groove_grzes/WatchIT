package com.watchit.servlets;

import com.watchit.DAO.MoviesDAO;
import com.watchit.model.Movie;
import com.watchit.servlets.other.MovieDataValidation;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@WebServlet("/manageMovies")
public class ManageMovies extends HttpServlet {

    @EJB
    private MoviesDAO moviesDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<Movie> listOfAllMovies = moviesDAO.getAllMovies();

        req.setAttribute("listOfAllMovies", listOfAllMovies);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("manageMovies.jsp");
        requestDispatcher.forward(req, resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        String title = req.getParameter("title");
        String director = req.getParameter("director");
        String genre = req.getParameter("genre");
        String description = req.getParameter("description");
        String yearOfMovie = req.getParameter("year");

        if(MovieDataValidation.isMovieDataValid(title, director, genre, description, yearOfMovie)){
            resp.sendRedirect(resp.encodeRedirectURL("movieAdd?error=dataError"));
        }else{
            int year = Integer.parseInt(yearOfMovie);
            moviesDAO.createMovie(new Movie(null ,title, genre, description, year, director));
            resp.sendRedirect(resp.encodeRedirectURL("manageMovies"));
        }
    }
}
