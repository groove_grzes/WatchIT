package com.watchit.servlets;

import com.watchit.DAO.MoviesDAO;
import com.watchit.model.MovieType;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/movieAdd")
public class MovieAdd extends HttpServlet {

    @EJB
    MoviesDAO moviesDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<MovieType> listOfMovieTypes = moviesDAO.movieTypes();

        req.setAttribute("listOfMovieTypes", listOfMovieTypes);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("movieAdd.jsp");
        requestDispatcher.forward(req, resp);
    }
}
