package com.watchit.servlets;

import com.watchit.DAO.UsersDAO;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/userDelete")
public class UserDelete extends HttpServlet {

    @EJB
    UsersDAO usersDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userLoginToDelete = req.getParameter("login");

        usersDAO.deleteUser(userLoginToDelete);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("users");
        requestDispatcher.forward(req, resp);

    }
}
