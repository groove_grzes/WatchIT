package com.watchit.servlets.other;

public class MovieDataValidation {

    public static boolean isMovieDataValid(String title, String director, String genre, String description, String yearOfMovie){

        return (title.isEmpty() || director.isEmpty() || genre.isEmpty() || description.isEmpty() || yearOfMovie.isEmpty() || genre.equals("Choose movie genre") || !YearValidation.isYearProper(yearOfMovie));
    }

}
