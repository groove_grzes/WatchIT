package com.watchit.servlets.other;

import java.util.regex.Pattern;

public class YearValidation {
    public static boolean isYearProper(String year){
//        Pattern checkYear = Pattern.compile("[1-2][890][0-9]{2}");
        Pattern checkYear = Pattern.compile("18[0-9]{2}|19[0-8][0-9]|199[0-9]|200[0-9]|201[0-8]");

        return checkYear.matcher(year).matches();
    }
}
