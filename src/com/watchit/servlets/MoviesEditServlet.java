package com.watchit.servlets;

import com.watchit.DAO.MoviesDAO;
import com.watchit.model.Movie;
import com.watchit.servlets.other.MovieDataValidation;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/moviesEdit")
public class MoviesEditServlet extends HttpServlet {

    @EJB
    private MoviesDAO moviesDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idToEdit = req.getParameter("id");

        Movie movieByIdToEdit = moviesDAO.getMovieById(Integer.parseInt(idToEdit));

        req.setAttribute("movieToEdit", movieByIdToEdit);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("movieEdit.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("UTF-8");

        String id = req.getParameter("id");
        String title = req.getParameter("title");
        String director = req.getParameter("director");
        String genre = req.getParameter("genre");
        String description = req.getParameter("description");
        String year = req.getParameter("year");

        if(MovieDataValidation.isMovieDataValid(title, director, genre, description, year)){

            resp.sendRedirect(resp.encodeRedirectURL("moviesEdit?error=dataError&id=" + id));

        }else{

            int idToUpdate = Integer.parseInt(id);
            int yearToUpdate = Integer.parseInt(year);
            Movie movieTOUpdate = new Movie(idToUpdate, title, genre, description, yearToUpdate, director);

            moviesDAO.updateMovie(movieTOUpdate);
            resp.sendRedirect(resp.encodeRedirectURL("manageMovies"));
        }
    }
}
