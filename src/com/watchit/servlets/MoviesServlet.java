package com.watchit.servlets;

import com.watchit.DAO.RentalsDAO;
import com.watchit.model.MovieRentedBy;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


@WebServlet("/movies")
public class MoviesServlet extends HttpServlet {

    @EJB
    RentalsDAO rentalsDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<MovieRentedBy> listOfMovies;

        if(req.getParameter("sortBy") == null){
            listOfMovies = rentalsDAO.getMovies((String)req.getSession().getAttribute("login"));
        }
        else if(req.getParameter("sortBy").equals("TitleAZ")){
            listOfMovies = rentalsDAO.getMoviesByTitleAZ((String)req.getSession().getAttribute("login"));
        }else if(req.getParameter("sortBy").equals("DirectorAZ")){
            listOfMovies = rentalsDAO.getMoviesByDirectorAZ((String)req.getSession().getAttribute("login"));
        }else if(req.getParameter("sortBy").equals("TitleZA")){
            listOfMovies = rentalsDAO.getMoviesByTitleZA((String)req.getSession().getAttribute("login"));
        }else if(req.getParameter("sortBy").equals("DirectorZA")){
            listOfMovies = rentalsDAO.getMoviesByDirectorZA((String)req.getSession().getAttribute("login"));
        }else{
            listOfMovies = rentalsDAO.getMovies((String)req.getSession().getAttribute("login"));
        }

        req.setAttribute("listOfMovies", listOfMovies);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("movies.jsp");
        requestDispatcher.forward(req, resp);

    }
}
