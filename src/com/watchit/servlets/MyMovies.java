package com.watchit.servlets;

import com.watchit.DAO.RentalsDAO;
import com.watchit.model.RentalDTO;

import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/myMovies")
public class MyMovies extends HttpServlet {

    @EJB
    private RentalsDAO rentalsDAO;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        List<RentalDTO> listOfAllMoviesByCustomer = rentalsDAO.getMoviesByUser((String) req.getSession().getAttribute("login"));

        req.setAttribute("listOfAllMoviesByCustomer", listOfAllMoviesByCustomer);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("mymovies.jsp");
        requestDispatcher.forward(req, resp);

    }
}
